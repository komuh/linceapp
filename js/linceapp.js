jQuery(window).ready(function ($) {


    var url = window.location.href;

    var init = function(){

        $(".menu-icon").on("click", showMenu);
        $(".close-menu").on("click", hideMenu);
        $(".item-terms-conditions, .menu-sub-item").on("click", showLightBoxes);
        $(".close-llight-box").on("click", hideLightBoxes);

        scrollControl();

        $(window).scroll(activateBgHeader);

        openLightBox();

        $(".btn-app-download").on("click", trackBtnDownload);

    };

    var showMenu = function(){

            $(".menu").css({
                "margin-right": "0"
            });

    };

    var hideMenu = function(){

            $(".menu").css({
                "margin-right": "-250px"
            });
    };

    var showLightBoxes = function(){

        if($(this).hasClass('use-conditions')){

            $(".light-box-use-conditions").fadeIn('slow');

        }else if($(this).hasClass('terms-conditions')){

            $(".light-box-terms-and-conditions-adicionais").fadeIn('slow');

        }else if($(this).hasClass('private-policts')){

            $(".light-box-private-politics").fadeIn('slow');

        }

    };

    var hideLightBoxes = function(){

        $(".light-box").fadeOut();

    };

    var scrollControl = function () {

        var target, scroll;

        $("a[href*=#]:not([href=#])").on("click", function (e) {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                target = $(this);
                scroll = ($("[id='" + target.attr("href").replace("#", "") + "']").offset().top);

                if (target.length) {
                    if (typeof document.body.style.transitionProperty === 'string') {
                        e.preventDefault();

                        var avail = $(document).height() - $(window).height();

                        if (scroll > avail) {
                            scroll = avail;
                        }

                        $("html, body").animate({
                            scrollTop: scroll - 114
                        }, 500).data("transitioning", true);
                    } else {
                        $("html, body").animate({
                            scrollTop: scroll
                        }, 500);
                        return;
                    }
                }
            }
        });

        $("html").on("transitionend webkitTransitionEnd msTransitionEnd oTransitionEnd", function (e) {
            if (e.target == e.currentTarget && $(this).data("transitioning") === true) {
                $(this).removeAttr("style").data("transitioning", false);
                $("html, body").scrollTop(scroll);
                return;
            }
        });

    };

    var activateBgHeader = function(){

        if ($(window).scrollTop() > 200) {

            $(".header").css('box-shadow', "rgb(0, 0, 0) 0px 86px 120px -55px inset");




    }else{


            $(".header").css('background-color', "transparent");



        }

    };


    var openLightBox = function(){

        if(url.indexOf('#termos-e-condicoes-adicionais-para-usuarios-ofertantes') !== -1)
        {
            $(".light-box-terms-and-conditions-adicionais").fadeIn('slow');

        }
    };


    var trackBtnDownload = function(){


        ga('send', 'event', 'Direcionamento para a google play', 'click', 'Botão para a google play');


    };


    init();
});