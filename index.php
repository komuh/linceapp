<!DOCTYPE html>
<html data-wf-site="56c75dbe92be8b00043cbf46" data-wf-page="56c75dbe92be8b00043cbf47">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">

    <meta content="Lince App" property="og:site_name">
    <meta content="object" property="og:type">
    <meta content="http://www.linceapp.com.br/images/img-post.png" property="og:image">
    <meta content="Descomplique a sua vida com o Lince" property="og:title">
    <meta content="http://www.linceapp.com.br/" property="og:url">
    <meta content="Agora, você não precisa se preocupar com mais nada quando usar um estacionamento.
Conheça o Lince e diga adeus aos tíquetes de papel dos estacionamentos.﻿" property="og:description">

    <meta charset="utf-8">
    <title>Lince App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="generator" content="Webflow">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/webflow.css">
    <link rel="stylesheet" type="text/css" href="css/lince-landing-page.webflow.css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]
            }
        });
    </script>
    <script type="text/javascript" src="js/modernizr.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65981693-2', 'auto');
  ga('send', 'pageview');

</script>



    <!-- Hotjar Tracking Code for http://www.linceapp.com.br -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:177630,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    
</head>

<body class="body">

<div class="light-box light-box-terms-and-conditions-adicionais">
    <div class="bg-light-box">
        <div class="content-light-box">
            <div class="w-clearfix header-light-box">
                <div class="close-llight-box"></div>
                <div class="title-holder-light-box">
                    <div class="title-light-box">Termos e condições adicionais</div>
                </div>
            </div>
            <div class="content-light-box-holder">
                <div class="holder-text-light-box">
                    <div class="light-box-text">

                        PARTE III
                        <br/>
                        <br/>
                        TERMOS E CONDIÇÕES ADICIONAIS PARA USUÁRIOS OFERTANTES
                        <br/>
                        <br/>
                        Quando o Usuário Ofertante estiver usufruindo das funcionalidades do Aplicativo para as quais é necessário estar “logado” em uma Conta de Usuário Ofertante, deverão ser observados estes Termos e Condições Adicionais Para Usuários Ofertante.
                        <br/>
                        <br/>
                        1.    SOBRE O LINCE
                        <br/>
                        <br/>
                        1.1.    Através de uma plataforma totalmente digital, o aplicativo Lince oferece ao Usuário Ofertante uma solução para divulgação e contratação do uso de vagas para veículos junto a interessados pela utilização de vagas para veículos. O Lince, portanto, não oferta e não detém as vagas disponibilizadas no Aplicativo, apenas intermediando a contratação de seu uso entre os Usuários e os Usuários Ofertantes.
                        <br/>
                        <br/>
                        1.2.   O Lince é remunerado por uma taxa de conveniência cobrada dos Usuários do Aplicativo que contratarem uma vaga de estacionamento.
                        <br/>
                        <br/>
                        1.3.   A cobrança da taxa de conveniência não resultará em acréscimo para os Usuários do Aplicativo, em função da prática de preços diferenciados para os mesmos.
                        <br/>
                        <br/>
                        1.4.   Nenhuma pessoa que não o Lince está autorizada a cobrar qualquer taxa em nome do Lince.
                        <br/>
                        <br/>
                        2.    CONDIÇÕES APLICÁVEIS AOS SERVIÇOS
                        <br/>
                        <br/>
                        2.1.    O Serviço
                        <br/>
                        <br/>
                        2.1.1.    Para realizar a divulgação de uma vaga para veículo, o Usuário Ofertante deverá possuir pleno domínio e/ou propriedade do local a ser disponibilizado no Aplicativo.
                        <br/>
                        <br/>
                        2.1.1.1.    O Usuário Ofertante responde civilmente por quaisquer danos causados aos Usuários nos termos dos arts 186 e 927 do Código Civil Brasileiro.
                        <br/>
                        <br/>
                        2.1.1.2.    O Usuário Ofertante é inteiramente responsável pela veracidade e completude dos dados fornecidos através do Aplicativo.
                        <br/>
                        <br/>
                        2.1.2.    Todas as informações relativas às vagas oferecidas são fornecidas pelo Usuário Ofertante sem qualquer envolvimento do Lince.
                        <br/>
                        <br/>
                        2.1.3.    Eventuais avaliações dos Usuários disponibilizadas no Aplicativo não têm qualquer participação ou interferência do Lince. O Lince não se responsabiliza por eventuais danos à imagem do Usuário Ofertante causados pelas avaliações dos Usuários do Aplicativo, não se comprometendo a editá-las ou removê-las do Aplicativo.
                        <br/>
                        <br/>
                        2.2.    O Lince não se responsabiliza por eventuais indisponibilidades ou falhas no Serviço, o que inclui, mas não se limita a indisponibilidades ou falhas que deem causa:
                        <br/>
                        <br/>
                        2.2.1.    A perda dos dados do Usuário Ofertante produzidos e armazenados no Serviço, motivo pelo qual é fortemente recomendado que o Usuário Ofertante realize constantemente back-ups de suas informações; e,
                        <br/>
                        <br/>
                        2.2.2.    A impossibilidade de conclusão de uma contratação, motivo pelo qual é fortemente recomendado que o Usuário Ofertante mantenha formas alternativas e redundantes para a disponibilização de vagas online.
                        <br/>
                        <br/>
                        2.3.    EXCLUSÕES DE RESPONSABILIDADE
                        <br/>
                        <br/>
                        2.3.1.    O USUÁRIO OFERTANTE EXPRESSAMENTE RECONHECE QUE O LINCE, POR APENAS FACILITAR A CONTRATAÇÃO DO USO DE VAGAS OFERECIDAS PELO USUÁRIO OFERTANTE, NÃO É RESPONSÁVEL POR QUESTÕES INTRÍNSECAS A CONTRATAÇÃO E UTILIZAÇÃO DAS VAGAS, O QUE INCLUI, MAS NÃO SE LIMITA A:
                        <br/>
                        <br/>
                        2.3.1.1.    COMPLETUDE DAS INFORMAÇÕES DISPONIBILIZADAS PELOS USUÁRIOS DO APLICATIVO.
                        <br/>
                        <br/>
                        2.3.1.2.    EFETIVA CAPACIDADE OU SOLVÊNCIA DOS USUÁRIOS DO APLICATIVO PARA A CONTRATAÇÃO DAS VAGAS DISPONIBILIZADAS NO APLICATIVO.
                        <br/>
                        <br/>
                        2.3.1.3.    DANOS EVENTUALMENTE CAUSADOS AO USUÁRIO OFERTANTE OU A TERCEIROS PELOS USUÁRIOS CONTRATANTES OU POR TERCEIROS NA CONTRATAÇÃO E UTILIZAÇÃO DAS VAGAS.
                        <br/>
                        <br/>
                        2.3.1.4.    INDENIZAÇÕES OU REPARAÇÕES DE DANOS SOFRIDOS PELO USUÁRIO OFERTANTE OU A TERCEIROS CAUSADOS PELOS USUÁRIOS CONTRATANTES OU POR TERCEIROS NA CONTRATAÇÃO E UTILIZAÇÃO DAS VAGAS.
                        <br/>
                        <br/>
                        2.3.2.    É EXPRESSAMENTE ASSEGURADO AO LINCE O DIREITO DE REGRESSO CASO O LINCE VENHA A ARCAR COM QUALQUER REPARAÇÃO RELATIVA AO ITEM 2.3.1.4 ACIMA.
                        <br/>
                        <br/>
                        2.4.    Conteúdo Gerado por Terceiros
                        <br/>
                        <br/>
                        2.4.1.    O Lince permite que o Usuário Ofertante acesse informações relativas ao veículo e ao Usuário contratante da utilização de vaga para veículo, tais como placa e tipo do veículo, nome do usuário, sua avaliação, dentre outras informações. O Usuário Ofertante expressamente reconhece que tais informações são providas por tais usuários do Aplicativo, sem qualquer envolvimento do Lince, de modo que o Lince não é responsável por danos gerados por tal conteúdo, nos termos artigo 18 da Lei n° 12.965/2014.
                        <br/>
                        <br/>
                        3.    REMUNERAÇÃO DOS USUÁRIOS OFERTANTES
                        <br/>
                        <br/>
                        3.1.    A remuneração do Usuário Ofertante decorrerá sempre e tão somente dos proventos direta e efetivamente auferidos pela comercialização das vagas disponibilizadas e contratadas através do Aplicativo.
                        <br/>
                        <br/>
                        3.1.1.    Os valores a serem pagos pelo uso das vagas disponibilizadas poderão variar em função das tabelas aplicáveis, período de utilização da vaga pelo Usuário Contratante, tamanho do veículo entre outras. O Usuário Ofertante deverá informar tais termos quando disponibilizar uma vaga no aplicativo, expressamente reconhecendo que tais termos influenciarão a remuneração que lhe será repassada pelo Aplicativo.
                        <br/>
                        <br/>
                        3.2.    A remuneração do Usuário Ofertante será repassada pelo Lince no prazo de até 35 (trinta e cinco) dias após a ocorrência das transações pelo Aplicativo.
                        <br/>
                        <br/>
                        3.2.1.    O valor da remuneração será repassado pelo Lince aos Usuários Ofertantes pelo meio eletrônico da empresa Cielo ou através de movimentações bancárias.
                        <br/>
                        <br/>
                        3.2.2.    Os valores repassados pelo Lince ao Usuário Ofertante sempre será feito líquido de comissões, taxas e encargos que devam, por força de lei ou contrato, ser retidos pelo Lince.
                        <br/>
                        <br/>
                        3.2.3.    A comissão do Lince será cobrada e retida no ato do pagamento pelos Usuários Contratantes. Tal comissão será sempre previamente informada ao Usuário Ofertante antes da disponibilização de uma nova vaga.
                        <br/>
                        <br/>
                        3.2.4.    O Lince se reserva o direito de cobrar do Usuário Ofertante quaisquer valores que tenham sido creditados pelo Lince ao Usuário Ofertantes, mas que tenham sido posteriormente debitados contra o Lince pelos meios de pagamento por motivo de contestação de transação, disputas ou fraude. Em tais casos, o Lince poderá, além de cobrar tais valores do Usuário Ofertante, também debitar ou reter recursos do saldo do Usuário Ofertante que viria a ser objeto de posterior repasse nos termos deste item 3.2.
                        <br/>
                        <br/>
                        3.3.    O Usuário Ofertante expressamente reconhece que não fará jus a qualquer remuneração decorrente de outras formas de receitas que possam ser implementadas pelo Lince ou em outros negócios desenvolvidos pelo Lince.
                        <br/>
                        <br/>
                        4.    CADASTRO E REGISTRO
                        <br/>
                        <br/>
                        4.1.    Quando o Usuário Ofertante realiza o seu cadastro no Aplicativo, o Usuário Ofertante atesta que todos os dados fornecidos pelo Usuário Ofertante são verdadeiros, completos e precisos.
                        <br/>
                        <br/>
                        4.1.1.    Prover informações incompletas, imprecisas ou falsas constitui violação dos deveres destes Termos e Condições de Uso, estando o Usuário Ofertante inteiramente responsável pelos danos a que tal violação der causa.
                        <br/>
                        <br/>
                        4.1.2.    O Lince pode necessitar de mais informações e documentos sobre o Usuário Ofertante a qualquer tempo, seja para contatar ou para conduzir diligências internas, caso em que o Usuário Ofertante será requerido a fornecê-las. Não fornecer prontamente tais informações e documentos quando requisitado constituirá violação destes Termos e Condições de Uso.
                        <br/>
                        <br/>
                        4.2.    As informações que o Usuário Ofertante utiliza para preencher o cadastro podem mudar, e o Usuário Ofertante assume o compromisso de mantê-las sempre atualizadas, alterando-as tão logo ocorra alguma modificação.
                        <br/>
                        <br/>
                        4.3.    Em caso de mudanças, as informações utilizadas para preenchimento do cadastro do Usuário Ofertante devem ser notificadas diretamente aos Usuários do Aplicativo pelo próprio Usuário Ofertante e sob sua total responsabilidade, sem qualquer envolvimento do Lince.
                        <br/>
                        <br/>
                        4.4.    O Lince poderá se valer de todas as formas lícitas para verificar, a qualquer tempo, se as informações que o Usuário Ofertante nos proveu são verdadeiras.
                        <br/>
                        <br/>
                        4.4.1.    Se o Lince constatar que suas informações são de alguma forma incompletas, imprecisas ou falsas, o Usuário Ofertante poderá ter sua conta suspensa ou cancelada, a exclusivo critério do Lince, sem prejuízos de outras medidas que sejam aplicáveis, caso em que não serão devidos quaisquer reembolsos.
                        <br/>
                        <br/>
                        4.5.    O cadastro no Aplicativo é intransferível.
                        <br/>
                        <br/>
                        4.6.    Cada Usuário Ofertante só poderá manter um único cadastro junto ao Aplicativo.
                        <br/>
                        <br/>
                        4.7.    O Usuário Ofertante que realizar o cadastro em nome de uma pessoa jurídica assume e declara possuir poderes para representa-la no tocante a estes Termos e Condições Adicionais Para Usuários Ofertantes. O Usuário Ofertante que realizar o cadastro em nome de pessoa jurídica responderá solidariamente perante o Lince por qualquer violação a estes termos ou a legislação aplicável.
                        <br/>
                        <br/>
                        4.8.    O Lince emprega as melhores práticas do mercado para garantir a segurança do cadastro do Usuário Ofertante, contudo, o sigilo e a segurança do nome de usuário e senha são de única e exclusiva responsabilidade do Usuário Ofertante. Nenhuma contratação poderá ser anulada ou cancelada em razão de má proteção do usuário sobre seus dados de acesso.

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="light-box light-box-use-conditions">
    <div class="bg-light-box">
        <div class="content-light-box">
            <div class="w-clearfix header-light-box">
                <div class="close-llight-box"></div>
                <div class="title-holder-light-box">
                    <div class="title-light-box">Termos e condições de uso</div>
                </div>
            </div>
            <div class="content-light-box-holder">
                <div class="holder-text-light-box">
                    <div class="light-box-text">
                        Última atualização destes Termos e Condições de Uso disponibilizada no Aplicativo em 23/09/2015.
                        <br/>
                        <br/>
                        LINCE (“Aplicativo”) é pertencente e operado pela SRGA Sistemas Ltda – ME. (“Sinergia”), microempresa nos termos da Lei Complementar nº 123/2006, constituída como sociedade empresária de responsabilidade limitada, registrada sob o CNPJ/MF 22.152.674/0001-44, com sede à Rua dos Aimores, 462/313 –, Funcionários Belo Horizonte, MG, CEP 30140-070. O Usuário pode entrar em contato conosco pelo e-mail: <contato@linceapp.com.br>.
                        <br/>
                        <br/>
                        O Lince oferece um serviço pelo qual usuários interessados na cotação e contratação do uso de vagas de estacionamento para veículos contratem estes serviços através da plataforma Lince (“Serviço”) junto a estabelecimentos comerciais de estacionamento ou a outras pessoas físicas ou jurídicas que tenham vagas disponíveis para uso de terceiros (“Ofertante” ou “Ofertantes”).
                        <br/>
                        <br/>
                        Estes Termos e Condições de Uso, em conjunto com a nossa Política de Privacidade http://linceapp.com.br/politica-de-privacidade, estabelecem os termos e condições aplicáveis à utilização de nosso Aplicativo e aos Serviços que serão prestados pelo Lince aos usuários. É muito importante que qualquer usuário leia e compreenda estas regras, bem como outras regras que possam ser aplicáveis.
                        <br/>
                        <br/>
                        É possível navegar em parte do Aplicativo sem estar registrado na conta de usuário, caso em que serão aplicáveis as Condições Gerais (Parte I abaixo).
                        <br/>
                        <br/>
                        Caso o usuário (neste caso “Você” ou “Usuário”) queira utilizar o Serviço para cotar e contratar a utilização de vagas para veículos, será requerida a realização de um cadastro para a criação de uma Conta de Usuário. Em tal caso, Você estará expressamente concordando e anuindo com os Termos e Condições Adicionais (Parte II, abaixo).
                        <br/>
                        <br/>
                        Estes Termos e Condições de Uso podem ser alterados a qualquer momento e o Usuário será previamente informado caso isso aconteça. Caso o usuário não concorde com alguma mudança feita nestes termos, recomendamos que encerre sua conta e interrompa a utilização do Aplicativo. Caso não encerre sua conta, considerar-se-ão aceitos estes Termos e Condições de Uso. A última versão destes Termos e Condições de Uso sempre estará disponível no link: http://linceapp.com.br/termos-e-condicoes-de-uso/. Salientamos que tais alterações serão aplicáveis aos serviços prestados ao usuário desde o momento em que forem disponibilizadas no Aplicativo. Algumas condições disponíveis em avisos legais em outros locais do Aplicativo podem substituir ou complementar estes Termos e Condições de Uso, desde que expressamente previsto em cada caso.
                        <br/>
                        <br/>
                        Qualquer pessoa que utilizar nossos serviços, o que inclui a simples visita ao Aplicativo, assume e expressamente concorda com estes Termos e Condições de Uso.
                        <br/>
                        <br/>
                        PARTE I
                        <br/>
                        <br/>
                        CONDIÇÕES GERAIS
                        <br/>
                        <br/>
                        1.    CAPACIDADE
                        <br/>
                        <br/>
                        1.1.    O Aplicativo é destinado a pessoas físicas capazes e devidamente autorizadas a conduzirem veículos.
                        <br/>
                        <br/>
                        1.2.    O Lince recomenda que a utilização do Aplicativo pelo Usuário não ocorra enquanto o Usuário conduz seu veículo.
                        <br/>
                        <br/>
                        2.    DISPONIBILIDADE DO APLICATIVO
                        <br/>
                        <br/>
                        2.1.    O acesso ao Aplicativo pode ser interrompido, suspenso ou ficar intermitente temporariamente, sem qualquer aviso prévio, em caso de falha de sistema, manutenção, alteração de sistemas ou por qualquer motivo que escape ao controle do Lince, sem que se faça devida qualquer reparação por parte desta.
                        <br/>
                        <br/>
                        3.    CONTEÚDO
                        <br/>
                        <br/>
                        3.1.    O Lince irá se esforçar para manter o conteúdo do Aplicativo atualizado e completo, livre de quaisquer defeitos ou vírus. Contudo, o Lince não é responsável por esses e outros possíveis problemas. O Lince poderá alterar o conteúdo do Aplicativo que lhe couber a qualquer momento, sem prévio aviso.
                        <br/>
                        <br/>
                        3.2.    Parte das informações disponíveis no Aplicativo são geradas e transmitidas ao Aplicativo por terceiros, sem o envolvimento ou responsabilidade do Lince. O Lince nunca irá alterar o conteúdo que for produzido por terceiros.
                        <br/>
                        <br/>
                        3.3.    O Usuário concorda em utilizar este Aplicativo de boa-fé, sem usar, carregar, transmitir ou enviar do Aplicativo ou para o Aplicativo qualquer material:
                        <br/>
                        <br/>
                        3.3.1.    Que seja de cunho violento ou ameaçador, difamatório, obsceno, ofensivo, pornográfico, abusivo, passível de incitar qualquer forma de ódio racial, discriminatório ou em violação de privacidade de terceiro.
                        <br/>
                        <br/>
                        3.3.2.    Para o qual não tenham sido obtidas pelo Usuário todas as licenças e/ou aprovações necessárias.
                        <br/>
                        <br/>
                        3.3.3.    Que constitua ou incite condutas que possam ser consideradas ilícitos criminais ou civis, que violem direitos de terceiros seja no Brasil ou no exterior ou que sejam meramente ilegais sob qualquer outra forma.
                        <br/>
                        <br/>
                        3.3.4.    Que sejam tecnicamente danosos, incluindo, mas não limitado a vírus de computador, macros, “cavalos de tróia”, worms, componentes maliciosos, dados corrompidos e outros programas ou dados de computador maliciosos ou que sejam projetados para interferir, interromper ou derrubar as operações normais de um computador.
                        <br/>
                        <br/>
                        3.3.5.    Que tenha como objetivo fraudar, dissimular ou comprometer a escolha dos usuários quanto à contratação dos Serviços pelo Aplicativo, seja pela criação de perfis falsos, violação de protocolos de segurança ou qualquer outro meio.
                        <br/>
                        <br/>
                        3.4.    O Usuário deve zelar para que o uso do Aplicativo e do Serviço não sejam prejudiciais à estabilidade e à disponibilidade destes. Caso isso ocorra, o Usuário poderá ser responsabilizado pelos prejuízos a que der causa.
                        <br/>
                        <br/>
                        3.5.    O Lince poderá ser requerido a cooperar com autoridades e com diligências judiciais que visem a identificar os usuários que atuem em descumprimento com as disposições deste item 3, caso em que cooperará.
                        <br/>
                        <br/>
                        3.6.    O Lince poderá, a qualquer momento e a seu exclusivo critério, adicionar ou remover conteúdos e funcionalidades do Aplicativo sem que isso caracterize, sob qualquer forma, ofensa aos direitos adquiridos do Usuário.
                        <br/>
                        <br/>
                        4.    DIREITOS DE PROPRIEDADE INTELECTUAL
                        <br/>
                        <br/>
                        4.1.    O uso comercial do nome, dos desenhos e da expressão “Lince” como nome empresarial, marca ou nome de domínio, bem como os conteúdos das telas relativas aos Serviços assim como os programas, bancos de dados, documentos e demais utilidades e aplicações são de propriedade do Lince e estão protegidos por todas as leis e tratados aplicáveis.
                        <br/>
                        <br/>
                        4.2.    O uso indevido e a reprodução total ou parcial dos conteúdos referidos no item 4.1 são proibidos. Caso deseje utilizar algum destes conteúdos, o Usuário deverá entrar em contato conosco antes de fazê-lo. Usar qualquer conteúdo aqui mencionado sem a prévia e expressa autorização do Lince poderá acarretar em responsabilizações penais e civis.
                        <br/>
                        <br/>
                        5.    LINKS
                        <br/>
                        <br/>
                        5.1.    Em virtude de parcerias ou por acreditarmos que possa ser do interesse do Usuário, poderemos “linkar” no Aplicativo, a nosso exclusivo critério, outros aplicativos e funcionalidades da Internet, sem que isso signifique que esses aplicativos sejam de propriedade ou operados pelo Lince
                        <br/>
                        <br/>
                        5.2.    O Lince não será responsável pelos conteúdos, práticas e serviços ofertados por estes aplicativos.
                        <br/>
                        <br/>
                        5.3.    A disponibilização de links para outros aplicativos não implica em relação de sociedade, de supervisão, empreendimento conjunto (joint venture), solidariedade ou de garantia do Lince para com esses aplicativos e seus conteúdos.
                        <br/>
                        <br/>
                        6.    PRIVACIDADE
                        <br/>
                        <br/>
                        6.1.    A privacidade do Usuário é muito importante para o Lince. Ao prover com suas informações, o Usuário nos autoriza a divulgar e/ou utilizar estas informações estritamente nos termos previstos na nossa Política de Privacidade, que está disponível no Aplicativo http://linceapp.com.br/politica-de-privacidade.
                        <br/>
                        <br/>
                        7.    RESCISÃO
                        <br/>
                        <br/>
                        7.1.    Para promover o bom funcionamento e qualidade dos serviços do Aplicativo, o Lince se reserva no direito de, sem a necessidade de notificação prévia, impedir ou interromper o acesso do Usuário que, segundo o Lince, estiver atuando de qualquer forma a violar qualquer disposição destes Termos e Condições de Uso, da Política de Privacidade ou de qualquer contrato celebrado por meio do Aplicativo.
                        <br/>
                        <br/>
                        7.2.    O Lince poderá interromper o acesso e uso do Serviço de qualquer Usuário que tenha decretada a sua falência ou que perca qualquer autorização necessária ao desenvolvimento de sua atividade.
                        <br/>
                        <br/>
                        8.    DEMAIS CONDIÇÕES
                        <br/>
                        <br/>
                        8.1.    O Lince é uma empresa brasileira e o Aplicativo e seus Serviços são criados e mantidos em fiel cumprimento às leis brasileiras e demais tratados que são incorporados à jurisdição brasileira. Caso o Usuário esteja usufruindo do acesso do Aplicativo fora do Brasil, o Usuário será responsável pelo cumprimento das leis locais, na medida em que forem aplicáveis.
                        <br/>
                        <br/>
                        8.2.    Caso o Lince deixe de exercer qualquer direito previsto nestes Termos e Condições de Uso, isto não deverá ser interpretado como uma renúncia, abdicação ou revogação de disposição constante destes Termos e Condições de Uso.
                        <br/>
                        <br/>
                        8.3.    Todos os itens destes Termos e Condições de Uso serão regidos pelas leis vigentes da República Federativa do Brasil. Para dirimir quaisquer controvérsias é eleito o Foro da Cidade de Belo Horizonte no Estado de Minas Gerais.
                        <br/>
                        <br/>
                        PARTE II
                        <br/>
                        <br/>
                        TERMOS E CONDIÇÕES ADICIONAIS
                        <br/>
                        <br/>
                        Quando o Usuário estiver usufruindo das funcionalidades do Aplicativo para as quais é necessário estar “logado” em uma Conta de Usuário, deverão ser observados estes Termos e Condições Adicionais.
                        <br/>
                        <br/>
                        9.    SOBRE O LINCE
                        <br/>
                        <br/>
                        9.1.    Através de uma plataforma totalmente digital, o Lince oferece ao Usuário uma solução para cotação e contratação de uso de vagas para veículos junto aos Ofertantes. O Lince, portanto, não oferta e não detém as vagas disponibilizadas no Aplicativo, apenas intermediando a contratação de seu uso entre os Usuários e os Usuários Ofertantes.
                        <br/>
                        <br/>
                        9.2.    O Lince é remunerado por uma taxa de conveniência cobrada dos Usuários do Aplicativo que contratarem as vagas de estacionamento.
                        <br/>
                        <br/>
                        9.3.    A cobrança da taxa de conveniência não resultará em acréscimo financeiro para os Usuários do Aplicativo, em função da prática de preços diferenciados para os mesmos.
                        <br/>
                        <br/>
                        9.4.    Nenhuma pessoa que não o Lince está autorizada a cobrar qualquer taxa em nome do Lince.
                        <br/>
                        <br/>
                        10.    CONDIÇÕES APLICÁVEIS AOS SERVIÇOS
                        <br/>
                        <br/>
                        10.1.    PAGAMENTO. O Serviço só poderá ser contratado mediante pré-pagamento, através de compra prévia de créditos via cartão de crédito devidamente habilitado no cadastro do Usuário. Caso haja excedente do valor comprado, os créditos virtuais ficarão disponíveis na conta do usuário para futura utilização.
                        <br/>
                        <br/>
                        10.1.1.    Para solicitar a cotação e contratação do uso de uma vaga para veículo, precisamos que o Usuário responda algumas perguntas sobre as características do veículo, tais como, o tipo do veiculo (moto, carro ou carro grande), sua placa e o nome do condutor.
                        <br/>
                        <br/>
                        10.1.1.1.    O valor, disponibilidade, localização e outras características das vagas oferecidas pelos Ofertantes dependerão das informações fornecidas pelo Usuário.
                        <br/>
                        <br/>
                        10.1.1.2.    O Usuário é inteiramente responsável pela veracidade e completude dos dados fornecidos através do Aplicativo.
                        <br/>
                        <br/>
                        10.1.2.    Todas as informações relativas às vagas e aos Ofertantes são fornecidas pelos Ofertantes sem qualquer envolvimento do Lince. O Usuário expressamente reconhece que nenhuma informação constante no Aplicativo irá substituir ou alterar de qualquer forma as condições gerais de contratação oferecidas pelos Ofertantes. É fortemente recomendável que o Usuário certifique-se de todas as condições estabelecidas pelos Ofertantes. As informações presentes no Aplicativo podem não corresponder à totalidade das regras aplicáveis na relação do Usuário com os Ofertantes.
                        <br/>
                        <br/>
                        10.1.3.    O Lince disponibiliza informações de Ofertantes que estejam previamente cadastrados no Aplicativo. O Usuário poderá encontrar no Aplicativo informações de estabelecimentos possuidores de estacionamento como atividade comercial, de estabelecimentos não possuidores da referida atividade comercial, ou de pessoas físicas detentoras de espaço disponível para veículos.
                        <br/>
                        <br/>
                        10.1.4.    Seja antes ou depois da contratação de um espaço disponível para estacionar através do Lince, o Usuário expressamente reconhece que o Lince não é e nem será responsável por qualquer monitoramento ou supervisão sobre as práticas e políticas adotadas pelos Ofertantes.
                        <br/>
                        <br/>
                        10.1.5.    Eventuais avaliações de outros usuários disponibilizadas no Aplicativo não têm qualquer participação ou interferência do Lince.
                        <br/>
                        <br/>
                        10.1.6.    Informações e avisos oferecidos pelo Lince ao Usuário com relação a mudanças nas condições gerais de contratação do uso de vagas já contratadas (e.g mudança de preço, de periodicidade da contratação e em outras características intrínsecas do serviço prestado pelos Ofertantes, etc.) devem ser sempre confirmados com os Ofertantes. O Lince não tem qualquer obrigação de oferecer ao Usuário tais informações, podendo fazer por mera liberalidade.
                        <br/>
                        <br/>
                        10.1.7.    O Lince não tem qualquer obrigação de notificar as mudanças mencionadas no item anterior, sendo sempre recomendável que o Usuário consulte de tempos em tempos eventuais alterações.
                        <br/>
                        <br/>
                        10.2.    Seleção dos Ofertantes
                        <br/>
                        <br/>
                        10.2.1.    Nosso sistema combina automaticamente diversas variáveis e campos para que o Usuário possa contratar as vagas mais adequadas à sua necessidade. Nosso sistema eletrônico e automático leva em conta, principalmente, a proximidade com a região que o Usuário declara precisar.
                        <br/>
                        <br/>
                        10.2.2.    Tal combinação é feita pelo Lince por mera liberalidade, e o Usuário expressamente reconhece que isso não significa qualquer tipo de endosso ou recomendação do Lince quanto à qualidade dos serviços prestados pelos Ofertantes.
                        <br/>
                        <br/>
                        10.3.    EXCLUSÕES DE RESPONSABILIDADE
                        <br/>
                        <br/>
                        10.3.1.    O USUÁRIO EXPRESSAMENTE RECONHECE QUE O LINCE, POR APENAS FACILITAR A CONTRATAÇÃO DO USO DE VAGAS OFERECIDAS PELOS OFERTANTES, NÃO É RESPONSÁVEL POR QUESTÕES INTRÍNSECAS A CONTRATAÇÃO E UTILIZAÇÃO DAS CONTRATAÇÕES, O QUE INCLUI, MAS NÃO SE LIMITA A:
                        <br/>
                        <br/>
                        10.3.1.1.    QUALIDADE, DISPONIBILIDADE OU COMPLETUDE DOS SERVIÇOS E/OU INFORMAÇÕES PRESTADOS PELOS OFERTANTES.
                        <br/>
                        <br/>
                        10.3.1.2.    EFETIVA DISPONIBILIDADE DO OFERTANTE PARA EFETUAR A CONTRATAÇÃO DE USO DE VAGA AO PREÇO QUE FOI DISPONIBILIZADO NO APLICATIVO.
                        <br/>
                        <br/>
                        10.3.1.3.    DANOS EVENTUALMENTE CAUSADOS AO USUÁRIO OU A TERCEIROS PELOS OFERTANTES OU POR TERCEIROS DURANTE A UTILIZAÇÃO DE VAGA PARA VEÍCULO.
                        <br/>
                        <br/>
                        10.3.1.4.    DANOS EVENTUALMENTE CAUSADOS AO VEÍCULO DO USUÁRIO PELOS OFERTANTES OU POR TERCEIROS DURANTE A UTILIZAÇÃO DE VAGA PARA VEÍCULO.
                        <br/>
                        <br/>
                        10.3.1.5.    INDENIZAÇÕES OU REPARAÇÕES DE DANOS SOFRIDOS PELO USUÁRIO OU POR TERCEIROS CAUSADOS PELOS OFERTANTES OU POR TERCEIROS NA CONTRATAÇÃO E UTILIZAÇÃO DAS VAGAS.
                        <br/>
                        <br/>
                        10.3.1.6.    INDENIZAÇÕES OU REPARAÇÕES DE DANOS SOFRIDOS PELO USUÁRIO OU POR TERCEIROS CAUSADOS PELO USUÁRIO OU POR TERCEIROS NA UTILIZAÇÃO INADEQUADA DO APLICATIVO.
                        <br/>
                        <br/>
                        10.3.2.    É EXPRESSAMENTE ASSEGURADO AO LINCE O DIREITO DE REGRESSO CASO ESTA VENHA A ARCAR COM QUALQUER REPARAÇÃO RELATIVA AOS ITENS 10.3.1.5 E 10.3.1.6 ACIMA.
                        <br/>
                        <br/>
                        10.4.    Conteúdo Gerado por Terceiros
                        <br/>
                        <br/>
                        10.4.1.    O Lince permite que o Usuário acesse informações relativas a estabelecimentos destinados a atividade comercial de estacionamentos, estabelecimentos não destinados à referida atividade comercial e pessoas físicas que possuem vagas disponíveis, tais como localização, espaço para o veículo através de foto, valores cobrados, horário de funcionamento, entre outras informações. O Usuário expressamente reconhece que tais informações são providas por tais estabelecimentos e pessoas físicas, sem qualquer envolvimento do Lince, de modo que o Lince não é responsável por danos gerados por tal conteúdo, nos termos artigo 18 da Lei n° 12.965/2014.
                        <br/>
                        <br/>
                        10.4.2.    O disposto no item anterior se aplica também a toda e qualquer informação sobre os espaços a serem locados, os estabelecimentos e pessoas físicas, bem como qualquer outra informação sobre os serviços do Aplicativo, que são providos pelos Ofertantes sem qualquer envolvimento do Lince.
                        <br/>
                        <br/>
                        10.5.    Cancelamentos
                        <br/>
                        <br/>
                        10.5.1.    O Usuário expressamente reconhece que o cancelamento do Serviço após a sua contratação implica no débito de metade do saldo restante a ser pago pelo Usuário.
                        <br/>
                        <br/>
                        11.    CADASTRO E REGISTRO
                        <br/>
                        <br/>
                        11.1.    Quando o Usuário realiza o seu cadastro no Aplicativo, o Usuário atesta que todos os dados fornecidos pelo Usuário são verdadeiros, completos e precisos.
                        <br/>
                        <br/>
                        11.1.1.    Prover informações incompletas, imprecisas ou falsas constitui violação dos deveres destes Termos e Condições de Uso, estando o Usuário inteiramente responsável pelos danos a que tal violação der causa.
                        <br/>
                        <br/>
                        11.1.2.    O Lince pode necessitar de mais informações e documentos sobre o Usuário a qualquer tempo, seja para melhor identificá-lo ou para conduzir diligências internas, caso em que o Usuário será requerido a fornecê-las. Não fornecer prontamente tais informações e documentos quando requisitado constituirá violação destes Termos e Condições de Uso.
                        <br/>
                        <br/>
                        11.2.    As informações que o Usuário utilizar para preencher o cadastro podem mudar, e o Usuário assume o compromisso de mantê-las sempre atualizadas, alterando-as tão logo ocorra alguma modificação.
                        <br/>
                        <br/>
                        11.3.    O Lince poderá se valer de todas as formas lícitas para verificar, a qualquer tempo, se as informações que o Usuário nos proveu são verdadeiras.
                        <br/>
                        <br/>
                        11.3.1.    Se o Lince constatar que suas informações são de alguma forma incompletas, imprecisas ou falsas, o Usuário poderá ter sua conta suspensa ou cancelada, a exclusivo critério do Lince, sem prejuízos de outras medidas que sejam aplicáveis, caso em que não serão devidos quaisquer reembolsos.
                        <br/>
                        <br/>
                        11.4.    O cadastro no Aplicativo é pessoal e intransferível.
                        <br/>
                        <br/>
                        11.5.    Cada Usuário só poderá manter um único cadastro junto ao Aplicativo.
                        <br/>
                        <br/>
                        11.6.    O Lince emprega as melhores práticas do mercado para garantir a segurança do cadastro do Usuário, contudo, o sigilo e a segurança do nome de usuário e senha são de única e exclusiva responsabilidade do Usuário. Nenhuma contratação poderá ser anulada ou cancelada em razão de má proteção do Usuário sobre seus dados de acesso.
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="light-box light-box-private-politics">
    <div class="bg-light-box">
        <div class="content-light-box">
            <div class="w-clearfix header-light-box">
                <div class="close-llight-box"></div>
                <div class="title-holder-light-box">
                    <div class="title-light-box">política de privacidade</div>
                </div>
            </div>
            <div class="content-light-box-holder">
                <div class="holder-text-light-box">
                    <div class="light-box-text">

                        Última atualização desta Política de Privacidade disponibilizada no Aplicativo em 23/09/2015. Esta é a primeira versão.
                        <br/>
                        <br/>
                        LINCE (“Aplicativo”) é pertencente e operado pela SRGA Sistemas Ltda. – ME (“Sinergia”), microempresa nos termos da Lei Complementar nº 123/2006, constituída como sociedade empresária de responsabilidade limitada, registrada sob o CNPJ/MF 22.152.674/0001-44, com sede à Rua dos Aimores,462/313, Belo Horizonte, MG, CEP 30140-070. O Usuário pode entrar em contato conosco pelo e-mail: <contato@linceapp.com.br>.
                        <br/>
                        <br/>
                        Esta Política de Privacidade, em conjunto com a nossos Termos e Condições de Uso http://linceapp.com.br/termos-e-condicoes-de-uso, estabelecem os termos e condições aplicáveis à utilização de nosso Aplicativo e aos serviços que serão prestados pelo Lince aos usuários. É muito importante que qualquer usuário leia e compreenda estas regras, bem como outras regras que possam ser aplicáveis.
                        <br/>
                        <br/>
                        Esta Política de Privacidade estabelece o compromisso do Lince com a sua privacidade. É muito importante que Você leia e compreenda estas regras, bem como outras regras que possam ser aplicáveis, incluindo aquelas advindas da Lei nº 8.078/1990 (“Código de Defesa do Consumidor”) e da Lei n° 12.965/2014 (“Marco Civil da Internet”).
                        <br/>
                        <br/>
                        Esta Política de Privacidade pode ser alterada a qualquer momento e o Usuário será previamente informado caso isso aconteça. Caso o Usuário não concorde com alguma mudança feita, recomendamos que encerre sua conta e interrompa a utilização do Aplicativo. A última versão desta Política de Privacidade sempre estará disponível no link: http://linceapp.com.br/politica-de-privacidade. Salientamos que tais alterações serão aplicáveis aos nossos serviços prestados ao Usuário desde o momento em que forem disponibilizadas no Aplicativo.
                        <br/>
                        <br/>
                        Qualquer pessoa que utilizar nossos serviços, o que inclui a simples visita ao Aplicativo, assume e expressamente concorda com a nossa Política de Privacidade.
                        <br/>
                        <br/>
                        Nesta Política de Privacidade, o Usuário encontrará as informações necessárias para entender sobre:
                        <br/>
                        <br/>
                        (i)    Quais informações coletamos sobre o Usuário;
                        <br/>
                        <br/>
                        (ii)    Como coletamos essas informações;
                        <br/>
                        <br/>
                        (iii)    Como interromper a coleta de informações;
                        <br/>
                        <br/>
                        (iv)    Como armazenamos e protegemos as informações coletadas;
                        <br/>
                        <br/>
                        (v)    Para que usamos essas informações; e,
                        <br/>
                        <br/>
                        (vi)    Em quais casos poderemos ceder ou divulgar estas informações.
                        <br/>
                        <br/>
                        I.    QUAIS INFORMAÇÕES COLETAMOS SOBRE O USUÁRIO
                        <br/>
                        <br/>
                        As informações que coletamos são as seguintes:
                        <br/>
                        <br/>
                        (a)    Informações Pessoais. Ao se cadastrar, poderemos coletar nome, e-mail, telefone de contato, placa e tipo do veículo, número do cartão de crédito e outras informações que podem ser necessárias para as solicitações do Usuário, bem como prestar-lhe outros serviços através do Aplicativo.
                        <br/>
                        <br/>
                        i.    O Usuário poderá efetuar o pagamento referente às contratações feitas através dos meios de pagamento disponíveis em nossa plataforma. Neste caso, os dados bancários ou de cartão de crédito que o Usuário irá fornecer serão coletados diretamente pelas administradoras de cartão de crédito, instituições financeiras e arranjos de pagamento, sem qualquer envolvimento do Lince. Não utilize um meio de pagamento caso não concorde com a forma como este serviço trata as suas informações.
                        <br/>
                        <br/>
                        (b)    Informações Não-Pessoais. Poderemos coletar informações e dados sobre a utilização da Plataforma Lince. Ao longo de sua visita no Aplicativo, poderão ser automaticamente coletados o Internet Protocol (IP), localização, tipo de telefone móvel, tipo de navegador, páginas e serviços acessados e/ou solicitados etc.
                        <br/>
                        <br/>
                        Poderemos cruzar Informações Pessoais com Informações Não-Pessoais. Neste caso, as Informações Não-Pessoais que forem relacionadas às suas Informações Pessoais serão tratadas como Informações Pessoais, para os propósitos desta Política de Privacidade.
                        <br/>
                        <br/>
                        Sempre que as nossas tecnologias permitirem identificar determinado Usuário através da análise isolada e independente de uma informação ou dado, tal informação ou dado será considerada, para os fins desta Política de Privacidade, uma Informação Pessoal.
                        <br/>
                        <br/>
                        II.    COMO COLETAMOS ESSAS INFORMAÇÕES
                        <br/>
                        <br/>
                        Nós poderemos coletar e armazenar informações sobre Você através:
                        <br/>
                        <br/>
                        (a)    Do seu cadastro na Plataforma Lince;
                        <br/>
                        <br/>
                        (b)    Do seu uso das funcionalidades da Plataforma Lince, em especial do seu preenchimento dos campos necessários para a conclusão de suas solicitações; e,
                        <br/>
                        <br/>
                        (c)    De Cookies. Cookies são identificações da interação com nosso Aplicativo que são transferidas para o aparelho do cliente visando reconhecê-lo na próxima navegação. Utilizamos cookies para proporcionar uma melhor experiência em nosso Aplicativo e viabilizar recursos personalizados como recomendações de serviços, publicidades e informações adicionais do seu interesse.
                        <br/>
                        <br/>
                        III.    COMO INTERROMPER A COLETA DE INFORMAÇÕES
                        <br/>
                        <br/>
                        Para não ter suas informações pessoais coletadas (mencionadas no item “I a” e “b”, acima), recomendamos fortemente que Você não as forneça. Neste caso, alertamos que a utilização do Serviço não será possível.
                        <br/>
                        <br/>
                        Os principais navegadores de Internet possibilitam aos seus clientes gerenciar a utilização dos cookies em seu computador ou telefone móvel. A nossa recomendação é que se mantenha o salvamento de cookies ligados. Desta forma, é possível explorar todos os recursos de navegação personalizada oferecidos no Aplicativo.
                        <br/>
                        <br/>
                        Caso deseje interromper a coleta de informações não pessoais (mencionadas no item “I c”, acima), o salvamento de cookies deverá ser desabilitado em seu navegador de Internet, os cookies salvos devem ser apagados e Você deverá gerenciar sua utilização por meio da configuração do navegador que utiliza para acessar o Aplicativo.
                        <br/>
                        <br/>
                        IV.    COMO ARMAZENAMOS AS INFORMAÇÕES COLETADAS
                        <br/>
                        <br/>
                        Após coletarmos os dados e informações mencionados nessa Política de Privacidade, iremos armazená-los sob as mais rígidas práticas de segurança de informação. Nosso banco de dados terá seu acesso criteriosamente restringido a apenas alguns funcionários habilitados, que são obrigados por contrato a preservar a confidencialidade de suas informações.
                        <br/>
                        <br/>
                        Iremos sempre empregar nossos melhores esforços para garantir que suas informações sejam sempre manipuladas de acordo com o estabelecido nesta Política de Privacidade. Ainda assim, Você deve saber que nós não somos responsáveis por eventual quebra de segurança de nossa base de dados que cause a divulgação ou acesso indevido de suas informações de usuário. Em tal caso, nenhuma compensação por parte do Lince será devida a Você.
                        <br/>
                        <br/>
                        V.    PARA QUE USAMOS ESSAS INFORMAÇÕES
                        <br/>
                        <br/>
                        Todas as informações coletadas pela Plataforma Lince servem para permitir que prestemos serviços cada vez melhores para os usuários.
                        <br/>
                        <br/>
                        Nós poderemos utilizar essas informações (pessoais ou não) principalmente para:
                        <br/>
                        <br/>
                        (a)    Concluir suas solicitações e permitir o uso de certas funcionalidades da Plataforma Lince;
                        <br/>
                        <br/>
                        (b)    Traçar perfis e tendências demográficas de uso da Plataforma Lince;
                        <br/>
                        <br/>
                        (c)    Entrar em contato com o Usuário para confirmar ou verificar as informações que nos foram fornecidas;
                        <br/>
                        <br/>
                        (d)    Garantir que a Plataforma Lince se mantenha sempre útil para o Usuário, o que poderá incluir a personalização e sugestões de conteúdos, produtos ou serviços;
                        <br/>
                        <br/>
                        (e)    Proteger a segurança e a integridade da nossa base de dados;
                        <br/>
                        <br/>
                        (f)    Conduzir diligências internas relativas aos negócios da Plataforma Lince; e,
                        <br/>
                        <br/>
                        (g)    Desenvolver, melhorar e oferecer serviços e produtos de terceiros.
                        <br/>
                        <br/>
                        VI.    EM QUAIS CASOS PODEREMOS CEDER OU DIVULGAR ESTAS INFORMAÇÕES
                        <br/>
                        <br/>
                        A viabilidade de certos serviços prestados pelo Lince só ocorre pelo compartilhamento de algumas dessas informações, o que fazemos com responsabilidade e seguindo rigorosos parâmetros. Abaixo, citamos os casos nos quais o compartilhamento de informações se faz necessário:
                        <br/>
                        <br/>
                        (a)    Conclusão de Solicitações de cotação e/ou de contratação: Para que os Usuários possam cotar e contratar vagas para veículos junto aos Ofertantes através da Plataforma Lince, teremos que enviar algumas das informações inseridas pelo Usuário a base de Ofertantes participantes da Plataforma Lince;
                        <br/>
                        <br/>
                        (b)    Novos negócios: No contínuo desenvolvimento do nosso negócio, processos de aquisição e fusão de empresas, estabelecimento de parcerias comerciais, joint ventures e outros negócios podem ocorrer. Nesses negócios, informações dos respectivos clientes também poderão ser transferidas, mas ainda assim, será mantida a Política de Privacidade;
                        <br/>
                        <br/>
                        (c)    Ordem judicial: O Lince pode compartilhar dados pessoais em caso de requisição judicial;
                        <br/>
                        <br/>
                        (d)    Mídia, Redes Sociais e Parcerias Comerciais: O Lince poderá compartilhar Informações Não Pessoais para gerar e divulgar estatísticas em redes sociais, na mídia ou junto a parceiros comerciais. Isso poderá incluir dados e tendências demográficas oriundos de informações pessoais e não pessoais de Usuários e de empresas cadastrados na Plataforma Lince. Em nenhum caso, na hipótese deste item, ocorrerá divulgação não-agregada de informações pessoais; e,
                        <br/>
                        <br/>
                        (e)    Com a autorização do cliente: em demais casos, havendo a necessidade de compartilhamento das informações, enviaremos ao Usuário uma notificação solicitando sua aprovação.

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="w-clearfix menu">
    <div data-ix="hide-menu" class="close-menu"></div>
    <ul class="w-list-unstyled menu-list">
        <li class="menu-item"><a class="item-menu-link" href="#">início</a>
        </li>
        <li class="menu-item"><a class="item-menu-link" href="#section-two">cinco motivos</a>
        </li>
        <li class="menu-item"><a class="item-menu-link" href="#section-three">como funciona</a>
        </li>
        <li class="menu-item"><a class="item-menu-link" href="#section-four">baixe agora</a>
        </li>
        <li class="menu-item"><a class="item-menu-link" href="#section-six">seja parceiro</a>
        </li>
        <li class="menu-item"><a class="item-menu-link" href="#">sobre o app </a>
            <br>
            &nbsp; &nbsp;<a class="menu-sub-item terms-conditions" href="#">termos de uso</a>
            <br>
            &nbsp; &nbsp;<a class="menu-sub-item use-conditions" href="#">condições de uso</a>
            <br>
            &nbsp; &nbsp;<a class="menu-sub-item" href="#section-seven">fale conosco</a>
        </li>
        <li class="menu-item"><a class="item-menu-link" href="http://blog.linceapp.com.br/" target="_blank">blog</a>
        </li>
    </ul>
    <div class="holder-social-media-menu">
        <div class="title-social-media-menu">compartilhe</div>
    </div>
    <div class="w-clearfix social-media-menu-icons-holder">


        <a class="icon-facebook" rel="nofollow"
           href="http://www.facebook.com/"
           onclick="popUp=window.open(
                       'http://www.facebook.com/sharer.php?u=http://www.linceapp.com.br/',
                       'popupwindow',
                       'scrollbars=yes,width=600,height=400');
                       popUp.focus();
                       return false">
            <div class="icon-social-media-menu"></div>
        </a>

    </div>
    <div class="title-download-menu">baixe</div>
    <a href="https://play.google.com/store/apps/details?id=br.com.sinergiasistemas.lincecliente" target="_blank">
        <div class="btn-app-download btn-app-download-menu"></div>
    </a>
</div>

<div class="w-section w-clearfix header">
    <div data-ix="show-menu" class="menu-icon"></div>
    <div class="logo-header"></div>
</div>

<div class="w-section section-top" id="section-one">
    <div class="w-container">
        <h1 class="title-section-top">Agora, você não precisa se preocupar com mais nada quando usar um estacionamento.</h1>

        <div class="separete-section-top"></div>
        <div class="subtitle-top-section">Busque, estacione, pague e tchau.</div>
        <div class="w-clearfix center-content-section-top">
        <a href="https://play.google.com/store/apps/details?id=br.com.sinergiasistemas.lincecliente" target="_blank">
            <img src="images/img-mobile-top.png" class="image-center-section-top">
        </a>
            <div class="text-content-center-top">
                <span class="title-top-section-thin">Deixe o <strong>Lince</strong><br></span>
                <span class="sub-text-content-center">descomplicar <br>a sua vida.</span>
            </div>

            <div class="tbns-app-download-holder">
                <div class="label-app-downloads">Baixe agora:</div>
                <a href="https://play.google.com/store/apps/details?id=br.com.sinergiasistemas.lincecliente" target="_blank">
                    <div class="btn-app-download"></div>
                </a>
            </div>
        </div>
    </div>
    <div class="know-more-section-top">conheça mais</div>
    <a href="#section-two">
        <div class="down-arrow-know-more"></div>
    </a>

    <div class="w-clearfix holder-social-media">
        <div class="label-social-media">Curta:</div>
        <div class="w-widget w-widget-facebook like-btn">
            <iframe src="https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Ffacebook.com%2Flinceapp&amp;layout=button_count&amp;locale=pt_BR&amp;action=like&amp;show_faces=false&amp;share=false"
                    scrolling="no" frameborder="0" allowtransparency="true" style="border: none; overflow: hidden; width: 90px; height: 20px;"></iframe>
        </div>


        <a class="icon-facebook" rel="nofollow"
           href="http://www.facebook.com/"
           onclick="popUp=window.open(
                       'http://www.facebook.com/sharer.php?u=http://www.linceapp.com.br',
                       'popupwindow',
                       'scrollbars=yes,width=600,height=400');
                       popUp.focus();
                       return false">
            <div class="social-media-share social-media-share-facebook"></div>
        </a>

        <div class="label-social-media label-social-media-share">Compartilhe:</div>
    </div>
    <div class="ticket-holder">
        <div class="w-container tickets-contaent-holder">
            <div class="text-section-tickets">Diga adeus aos
                <br>tíquetes de papel dos
                <br>estacionamentos
            </div>
            <div class="downloads-holder-tickets"><img src="images/white-hand.png" class="img-hand">

                <div class="title-download-tickets-section">Baixe agora o <strong>Lince</strong>
                    <br>no seu celular!
                </div>
                <a href="https://play.google.com/store/apps/details?id=br.com.sinergiasistemas.lincecliente" target="_blank">
                    <div class="btn-app-download transparent-btn-google-play"></div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="w-section sectin-five-reasons" id="section-two">
    <h2 class="title-section-five-reasons">para você<br>amar o Lince</h2>

    <div class="w-container holder-five-reasons">
        <div class="w-row five-steps-row">
            <div class="w-col w-col-4 w-col-small-4">
                <div class="img-reason"></div>
                <div class="text-reason">Tenha menos stresse
                    <br>na sua vida.
                </div>
            </div>
            <div class="w-col w-col-4 w-col-small-4">
                <div class="img-reason img-reason-two"></div>
                <div class="text-reason">Encontre a melhor
                    <br>vaga para você.
                </div>
            </div>
            <div class="w-col w-col-4 w-col-small-4">
                <div class="img-reason img-reason-three"></div>
                <div class="text-reason">Compare preços
                    <br>e economize.
                </div>
            </div>
        </div>
        <div class="row-two-reasons-holder">
            <div class="w-row five-steps-row-two">
                <div class="w-col w-col-6 w-col-small-6">
                    <div class="img-reason img-reason-four"></div>
                    <div class="text-reason">Acompanhe o quanto
                        <br>vai pagar pelo aplicativo.
                    </div>
                </div>
                <div class="w-col w-col-6 w-col-small-6">
                    <div class="img-reason img-reason-five"></div>
                    <div class="text-reason">Pague direto no celular
                        <br>e não ande com dinheiro.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w-section section-how-it-work" id="section-three">
    <h2 class="title-section-how-it-work">Como Funciona</h2>

    <div class="w-container content-how-it-work-holder">
        <div class="w-clearfix how-it-hork-content-holder">
            <div class="how-it-work-item">
                <div class="how-it-work-item-img"></div>
                <div class="how-it-work-item-title">Busque</div>
                <div class="how-it-work-item-description">Com o <strong>Lince,</strong> você encontra o melhor estacionamento.
                </div>
            </div>
            <div class="how-it-work-item how-it-work-item-separator">
                <div class="how-it-work-item-img how-it-work-item-separator-img"></div>
            </div>
            <div class="how-it-work-item">
                <div class="how-it-work-item-img how-it-work-item-img-two"></div>
                <div class="how-it-work-item-title">Estacione</div>
                <div class="how-it-work-item-description">Faça o check-in quando estiver dentro do estacionamento para iniciar a cobrança.

                </div>
            </div>
            <div class="how-it-work-item how-it-work-item-separator">
                <div class="how-it-work-item-img how-it-work-item-separator-img"></div>
            </div>
            <div class="how-it-work-item">
                <div class="how-it-work-item-img how-it-work-img-three"></div>
                <div class="how-it-work-item-title">Pague</div>
                <div class="how-it-work-item-description">O pagamento é feito no aplicativo utilizando o seu cartão com total segurança.</div>
            </div>
        </div>
    </div>
</div>
<div class="w-section section-where-to-go" id="section-four">
    <h2 class="ttle-where-to-go">Saiba onde vai estacionar e não<br>se atrase para compromissos.</h2>

    <div class="text-where-to-go">O <strong>Lince</strong> traça a rota até o estacionamento que você escolheu.</div>
</div>
<div class="w-section section-no-worries-with-change" id="section-five">
    <div class="w-container content-no-worries-holder">
        <h1 class="title-section-no-worries"><span class="bg-title-no-worries-section"> Não se <br> preocupe <br> com o troco <br> nunca mais! <br> </span></h1><img src="images/img-mobile-center.png"
                                                                                                                                                                     class="no-worries-image-center">

        <div class="tbns-app-download-holder btns-download-holder-noworries"><img src="images/white-hand.png" class="img-hand">

            <div class="title-download-tickets-section text-execute-the-download text-download-section-no-worries">Baixe agora o Lince
                <br>no seu celular
            </div>
            <a href="https://play.google.com/store/apps/details?id=br.com.sinergiasistemas.lincecliente" target="_blank">
                <div class="btn-app-download btn-download-google-noworries"></div>
            </a>
        </div>
    </div>
</div>
<div class="w-section section-parking-lot-owner" id="section-six">
    <div class="w-container">
        <h2 class="title-section-parking-lot-owner"><span class="title-parking-lot-bg"> é dono de um <br> estacionamento? </span></h2>

        <div class="subtitle-parking-lot-owner">Saiba por que o <strong>lince</strong> também vai resolver a sua vida.</div>
        <div class="content-holder-pl-owner">
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">Gestão remota</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">Você pode fazer a gestão de qualquer
                            <br>lugar e ter 100% de controle.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">visibilidade</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">O seu estacionamento vai ser
                            <br>conhecido por mais pessoas.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">mais clientes</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">Aumentamos as chances de melhorar
                            <br>a sua taxa de ocupação.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">segurança</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">Diminuímos a circulação de
                            <br>dinheiro vivo no seu caixa.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">cartão de
                            <br>crédito
                        </div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">O pagamento é feito no aplicativo, com
                            <br>cartão de crédito, e você não se preocupa
                            <br>com o troco.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">CONTROLE</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">Você escolhe os horários em que
                            <br>as vagas estarão disponíveis.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">autonomia</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">Seus funcionários escolhem
                            <br>quando aceitar as reservas.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">Facilidade</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">É fácil de usar: tudo em 2 cliques.</div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">Automatização</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">Não representa mais trabalho
                            <br>para o seu estacionamento.
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-parking-lot-owner last-item-pl-owner">
                <div class="w-row">
                    <div class="w-col w-col-3 w-clearfix">
                        <div class="item-pl-owner-title">parceria</div>
                    </div>
                    <div class="w-col w-col-9 w-clearfix">
                        <div class="item-pl-owner-description">Atraímos novos usuários e não competimos
                            <br>com os seus clientes atuais.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w-section section-footer" id="section-seven">
    <div class="div-road-footer">
        <div class="w-container content-holder-road-div-footer">
            <div class="title-section-footer"><span class="title-section-footer-different-bg"> &nbsp;cadastre&nbsp;</span>&nbsp;seu
                <br>estacionamento
                <br>e comece a ganhar
                <br>novos clientes agora!
            </div>
            <div class="tbns-app-download-holder btns-download-holder-noworries btns-download-holder-footer"><img src="images/big-logo-lince.png" class="big-logo-lince-footer">

                <div class="title-download-tickets-section text-execute-the-download title-download-app-footer">Baixe o Lince!</div>
                <a href="https://play.google.com/store/apps/details?id=br.com.sinergiasistemas.lincecliente" target="_blank">
                    <div class="btn-app-download btn-download-google-noworries btn-download-google-footer"></div>
                </a>
            </div>
        </div>
    </div>
    <div class="div-blue-footer">
        <div class="w-container content-holder-footer"><img width="115" src="images/complete-logo-lince-black.png" class="logo-lince-footer">

            <div class="div-holder-tel-footer">
                <div class="title-tel-footer">Entre em contato:</div>
                <div class="tel-number-footer">+55 31 4042 0510</div>
                <div class="contact-footer">contato@linceapp.com.br</div>
            </div>
            <div class="terms-conditions-footer">
                <div class="terms-conditions-title">Termos e condições</div>
                <div class="item-terms-conditions use-conditions">Condições de uso</div>
                <div class="item-terms-conditions item-terms-conditions-two-lines terms-conditions">Termos adicionais para <br>usuários ofertantes</div>
                <div class="item-terms-conditions private-policts">Política de Privacidade</div>
            </div>
            <a href="#section-one" class="go-up-btn-footer"></a>

            <div class="w-clearfix lince-copyright">
                <div class="text-copyright">© Lince 2016 - Deixe o Lince descomplicar a sua vida.</div>
                <a href="https://www.facebook.com/linceapp/" target="_blank">
                    <div class="img-facebook-footer"></div>
                </a>

                <div class="text-facebook"><strong data-new-link="true">Siga nossa página</strong> no Facebook.</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/linceapp.js"></script>
<!--[if lte IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
</body>
</html>